﻿using UnityEngine;
using System.Collections;

public class CreditsScene : MonoBehaviour
{

	/// <summary>
	/// Back button press processing.
	/// </summary>
	public void DidPressBackButton()
	{

		StartCoroutine(GeneralScene.AsyncMapLoad(GeneralScene.SceneMain));

	}

}
