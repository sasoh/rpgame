﻿using UnityEngine;
using System.Collections;

public class SplashScene : MonoBehaviour
{

	public float SplashDuration = 1.0f;
	float TimePassed = 0.0f;

	// Use this for initialization
	void Start()
	{

		Debug.Log("Loading timeout start with " + SplashDuration + " seconds delay.");

	}

	// Update is called once per frame
	void Update()
	{

		UpdateTimePassed();
		CheckSceneLoad();

		// skip if any key pressed
		if (Input.anyKey == true)
		{
			TimePassed = SplashDuration;
		}

	}

	/// <summary>
	/// Updates time passed since scene appearance.
	/// </summary>
	void UpdateTimePassed()
	{

		TimePassed += Time.deltaTime;

	}

	void CheckSceneLoad()
	{

		if (TimePassed >= SplashDuration)
		{
			StartCoroutine(GeneralScene.AsyncMapLoad(GeneralScene.SceneMain));
		}

	}

}
