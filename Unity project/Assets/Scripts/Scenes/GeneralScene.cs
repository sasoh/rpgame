﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Contains general scene related information
/// </summary>
public class GeneralScene : MonoBehaviour
{

	// Static strings with scene names
	public static string SceneCharacterSelection = "CharacterSelection";
	public static string SceneCredits = "Credits";
	public static string SceneMain= "Main";
	public static string SceneSplash = "Splash";
	public static string SceneMap01 = "Map01Entrance";
	public static string SceneMap02 = "Map02Tutorial";
	public static string CurrentScene = "";

	public static IEnumerator AsyncMapLoad(string sceneName)
	{

		AsyncOperation LoadingOperation = Application.LoadLevelAsync(sceneName);
		if (LoadingOperation.isDone == false)
		{
			// still loading, maybe show loading indication
			// LoadingOperation.progress for [0; 1] loading status value
			yield return null;
		}
		CurrentScene = sceneName;

	}

}
