﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Highlights given scene object & scales it up a bit. Requires collider to detect mouse enter/exit events.
/// </summary>
public class HighlightOnMouseOverScript : MonoBehaviour
{

	/// <summary>
	/// Scene object to highlight on mouse over.
	/// </summary>
	public GameObject SceneObject = null;
	Renderer currentRenderer = null;
	
	Material OriginalMaterial;
	public Material OutlineMaterial;
	
	void Start()
	{

		Debug.Assert(SceneObject != null, "Scene object not set for highlight script.");

		currentRenderer = SceneObject.GetComponent<Renderer>();
		Debug.Assert(currentRenderer != null, "Failed to get current renderer.");

		OriginalMaterial = currentRenderer.material;

	}

	void OnMouseEnter()
	{

		currentRenderer.material = OutlineMaterial;

	}

	void OnMouseExit()
	{

		currentRenderer.material = OriginalMaterial;

	}

}
