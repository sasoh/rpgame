﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Controls applied on any map.
/// </summary>
public class GeneralMap : MonoBehaviour
{

	void Start()
	{

		// remove zoom key from playerprefs
		if (GeneralScene.CurrentScene == GeneralScene.SceneMap01)
		{
			if (PlayerPrefs.HasKey("zoomLevel") == true)
			{
				PlayerPrefs.DeleteKey("zoomLevel");
				PlayerPrefs.Save();
			}
		}

	}

	// Update is called once per frame
	void Update()
	{

		if (Input.GetKeyDown("escape") == true)
		{
			// load main scene
			Application.LoadLevel(GeneralScene.SceneMain);
		}

	}
}
