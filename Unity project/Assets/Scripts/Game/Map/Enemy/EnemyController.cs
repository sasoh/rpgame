﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour
{

	public float Health = 10.0f;

	Transform TargetTransform;
	NavMeshAgent navAgent;

	// Use this for initialization
	void Start()
	{

		navAgent = GetComponent<NavMeshAgent>();
		Debug.Assert(navAgent != null);

		TargetTransform = transform;

	}

	// Update is called once per frame
	void Update()
	{

		navAgent.SetDestination(TargetTransform.position);

	}

	public void Hit(float damage)
	{

		Health -= damage;

		if (Health <= 0.0f)
		{
			// die
			Destroy(gameObject);
		}

	}

	void OnTriggerEnter(Collider other)
	{

		GameObject otherObject = other.gameObject;
		if (otherObject.layer == LayerMask.NameToLayer("Player"))
		{
			// player inside range, start following
			TargetTransform = otherObject.transform;
		}

	}

}
