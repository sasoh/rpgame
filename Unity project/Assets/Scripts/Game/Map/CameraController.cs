﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{

	public float Boundary = 50.0f;
	public float speed = 5.0f;

	public int MinimalZoomLevel = 30;
	public int MaximalZoomLevel = 50;

	Vector3 OriginalPlayerPositionDifference;

	// player object position difference needed for recentering
	public GameObject PlayerObject;

	// Use this for initialization
	void Start()
	{

		OriginalPlayerPositionDifference = PlayerObject.transform.position - transform.position;

		LoadZoom();

	}

	// Update is called once per frame
	void Update()
	{

		CheckMovement();

	}


	void CheckMovement()
	{

		// edge & key horizontal/vertical movement
		float horizontal = Input.GetAxis("Horizontal");
		float vertical = Input.GetAxis("Vertical");

		Vector3 newPosition = transform.position;
		if ((Input.mousePosition.x > Screen.width - Boundary) || (horizontal > 0.1f))
		{
			newPosition.x += speed * Time.deltaTime; // move on +X axis
		}

		if ((Input.mousePosition.x < 0 + Boundary) || (horizontal < -0.1f))
		{
			newPosition.x -= speed * Time.deltaTime; // move on -X axis
		}

		if ((Input.mousePosition.y > Screen.height - Boundary) || (vertical > 0.1f))
		{
			newPosition.z += speed * Time.deltaTime; // move on +Z axis
		}

		if ((Input.mousePosition.y < 0 + Boundary) || (vertical < -0.1f))
		{
			newPosition.z -= speed * Time.deltaTime; // move on -Z axis
		}

		// centering on player
		if (Input.GetKeyDown("h") == true)
		{
			newPosition = PlayerObject.transform.position;
			newPosition -= OriginalPlayerPositionDifference;
		}

		transform.position = newPosition;

		HandleZoom();

	}

	/// <summary>
	/// Handles zoom logic.
	/// </summary>
	void HandleZoom()
	{

		// zoom setting
		float zoomLevel = Input.GetAxis("Mouse ScrollWheel");
		float currentFov = Camera.main.fieldOfView;
		currentFov -= zoomLevel * 10;
		currentFov = Mathf.Max(currentFov, (float)MinimalZoomLevel, 1.0f);
		currentFov = Mathf.Min(currentFov, (float)MaximalZoomLevel);
		Camera.main.fieldOfView = currentFov;

		SaveZoom();

	}

	/// <summary>
	/// Loads last zoom value from player preferences.
	/// </summary>
	void LoadZoom()
	{

		if (PlayerPrefs.HasKey("zoomLevel") == true)
		{
			int zoomLevel = PlayerPrefs.GetInt("zoomLevel");
			Camera.main.fieldOfView = zoomLevel;
		}

	}

	/// <summary>
	/// Saves last zoom value to player preferences.
	/// </summary>
	void SaveZoom()
	{

		PlayerPrefs.SetInt("zoomLevel", (int)Camera.main.fieldOfView);
		PlayerPrefs.Save();

	}

}
