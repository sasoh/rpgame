﻿using UnityEngine;
using System.Collections;

public class EntranceDoorTriggerController : InteractableObjectScript
{

	/// <summary>
	/// Nav mesh obstacle game object.
	/// </summary>
	public GameObject DoorGameObject = null;

	/// <summary>
	/// Objects to move for effect.
	/// </summary>
	public GameObject MovingObject;

	/// <summary>
	/// Movement speed multiplier.
	/// </summary>
	public float MovementSpeed = 1.0f;
	private int movementDirection = 1;
	public float MaximumOffset = 1.0f;
	float orignalPositionY;

	public float RotationSpeed = 1.0f;

	void Start()
	{

		if (MovingObject != null)
		{
			orignalPositionY = MovingObject.transform.position.y;
		}

	}

	public override void DidReachInteractableObject()
	{

		// destroy door
		Destroy(DoorGameObject);

	}

	void Update()
	{

		AnimateEffect();

	}

	/// <summary>
	/// Gives slight rotation to sphere game objects.
	/// </summary>
	void AnimateEffect()
	{

		if (MovingObject != null)
		{
			Vector3 newPosition = MovingObject.transform.position;
			if ((newPosition.y > orignalPositionY + MaximumOffset) || (newPosition.y < orignalPositionY - MaximumOffset))
			{
				movementDirection *= -1;
			}
			newPosition.y += movementDirection * MovementSpeed * Time.deltaTime;
			
			MovingObject.transform.position = newPosition;			

			// animate rotation
			MovingObject.transform.Rotate(new Vector3(0.0f, RotationSpeed * Time.deltaTime, 0.0f));
		}

	}

}
