﻿using UnityEngine;
using System.Collections;

public class TrapSpikeScript : MonoBehaviour, ITrapTriggered
{

	public float Damage = 5.0f;
	public float Speed = 1.0f;
	public float LaunchDelay = 0.25f;
	
	int movementDirection = 0;
	float MaximumHeight = 0.0f;
	float MinimumHeight = 0.0f;
	float currentDelay = 0.0f;

	public void DidEnterTrap(TrapTriggerScript triggerScript)
	{

		MeshRenderer mRenderer = GetComponent<MeshRenderer>();

		MinimumHeight = transform.position.y;
		MaximumHeight = transform.position.y + mRenderer.bounds.size.y;

		currentDelay = LaunchDelay;
		movementDirection = 1;

	}

	void Update()
	{

		currentDelay -= Time.deltaTime;
		if (currentDelay <= 0.0f)
		{
			Vector3 newPosition = transform.position;
			if (movementDirection == 1)
			{
				if (transform.position.y < MaximumHeight)
				{
					newPosition.y += Speed * Time.deltaTime;
					newPosition.y = Mathf.Min(newPosition.y, MaximumHeight);
				}
				else
				{
					movementDirection = -1;
				}
			}
			else if (movementDirection == -1)
			{
				if (transform.position.y > MinimumHeight)
				{
					newPosition.y -= Speed * Time.deltaTime;
					newPosition.y = Mathf.Max(newPosition.y, MinimumHeight);
				}
				else
				{
					movementDirection = 0;
				}
			}
			transform.position = newPosition;
		}

	}

	void OnTriggerEnter(Collider other)
	{

		// player-only collisions for now!
		if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
		{
			// hurt player!
			PlayerController player = other.gameObject.GetComponent<PlayerController>();
			player.Health -= Damage;
		}

	}

}
