﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Interface for objects that are triggered by a trap.
/// </summary>
public interface ITrapTriggered
{

	/// <summary>
	/// Invoked when a trap was triggered.
	/// </summary>
	/// <param name="triggerScript"></param>
	void DidEnterTrap(TrapTriggerScript triggerScript);

}

public class TrapTriggerScript : MonoBehaviour
{

	/// <summary>
	/// Object that will be notified that the trap was triggered.
	/// </summary>
	public List<ITrapTriggered> TriggeredObjects = null;

	/// <summary>
	/// Spikes parent object.
	/// </summary>
	public GameObject SpikesParentObject = null;

	void Start()
	{

		InitSpikes();

	}

	void InitSpikes()
	{

		if (SpikesParentObject != null)
		{
			TrapSpikeScript[] spikes = SpikesParentObject.GetComponentsInChildren<TrapSpikeScript>();

			TriggeredObjects = new List<ITrapTriggered>();
			TriggeredObjects.AddRange(spikes);
		}

	}

	void OnTriggerEnter(Collider other)
	{

		// player-only collisions for now!
		if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
		{
			Debug.Log("Player triggered trap.");
			if (TriggeredObjects != null)
			{
				foreach (ITrapTriggered triggered in TriggeredObjects)
				{
					triggered.DidEnterTrap(this);
				}
			}
		}

	}

	void OnTriggerExit(Collider other)
	{


	}

}
