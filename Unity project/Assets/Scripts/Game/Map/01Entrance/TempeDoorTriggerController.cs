﻿using UnityEngine;
using System.Collections;

public class TempeDoorTriggerController : MonoBehaviour
{


	void OnTriggerEnter(Collider other)
	{
		
		if (other.gameObject.layer == LayerMask.NameToLayer("Player") == true)
		{
			// load next level
			StartCoroutine(GeneralScene.AsyncMapLoad(GeneralScene.SceneMap02));
		}

	}

}
