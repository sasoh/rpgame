﻿using UnityEngine;
using System.Collections;

public class InteractableObjectScript : MonoBehaviour
{

	public PlayerControllerInput PlayerMovementController = null;

	bool inContact = false;

	// Use this for initialization
	void Start()
	{
		
		inContact = false;
		Debug.Assert(PlayerMovementController != null, "Player movement controller not set!");

	}

	// Update is called once per frame
	void Update()
	{

	}

	void OnMouseOver()
	{

		if (Input.GetMouseButtonDown(1) == true)
		{
			PlayerMovementController.TargetGameObject = gameObject;

			// check if within trigger range
			if (inContact == true)
			{
				PlayerReached();
			}
		}

	}

	void PlayerReached()
	{

		if (PlayerMovementController.TargetGameObject == gameObject)
		{
			Debug.Log("Player interacted with " + gameObject.name);
			PlayerMovementController.TargetGameObject = null;
			DidReachInteractableObject();
		}

	}

	void OnTriggerEnter(Collider other)
	{

		if (other.gameObject == PlayerMovementController.gameObject)
		{
			inContact = true;

			PlayerReached();
		}

	}

	void OnTriggerExit(Collider other)
	{

		if (other.gameObject == PlayerMovementController.gameObject)
		{
			inContact = false;
		}

	}

	public virtual void DidReachInteractableObject()
	{

		// for overriding

	}

}
