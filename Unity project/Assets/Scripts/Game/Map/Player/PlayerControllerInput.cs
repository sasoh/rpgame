﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Handles input & input data for player game object.
/// </summary>
public class PlayerControllerInput : MonoBehaviour
{

	/// <summary>
	/// Movement speed multiplier.
	/// </summary>
	public float Speed = 1.0f;

	/// <summary>
	/// The rigidbody component.
	/// </summary>
	Rigidbody rb;

	/// <summary>
	/// Navigation mesh agent.
	/// </summary>
	NavMeshAgent navAgent = null;
	Vector3 navTarget;
	bool hasNewWaypoint = false;

	/// <summary>
	/// Player animator.
	/// </summary>
	public Animator PlayerAnimator = null;

	/// <summary>
	/// Game object towards which the player is headed. Used by interactable objects.
	/// </summary>
	GameObject targetGameObject = null;
	public GameObject TargetGameObject
	{
		get
		{
			return targetGameObject;
		}
		set
		{
			targetGameObject = value;

			Vector3 newTarget;
			if (targetGameObject != null)
			{
				newTarget = targetGameObject.transform.position;
			}
			else
			{
				// set current game object position so movement stops
				newTarget = transform.position;
			}

			navTarget = newTarget;
			hasNewWaypoint = true;
		}
	}

	void Start()
	{

		rb = GetComponent<Rigidbody>();
		Debug.Assert(rb != null, "No rigidbody component.");

		navAgent = GetComponent<NavMeshAgent>();
		Debug.Assert(navAgent != null, "No navmesh component.");

		Debug.Assert(PlayerAnimator != null);

	}

	void Update()
	{

		Move();

	}

	void Move()
	{

		if (hasNewWaypoint == true)
		{
			hasNewWaypoint = false;
			navAgent.SetDestination(navTarget);
		}
		else
		{
			if (Input.GetMouseButtonDown(1) == true)
			{

				RaycastHit hit;
				// destination to the point where the click occurred.
				var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

				if (Physics.Raycast(ray, out hit) == true)
				{
					TargetGameObject = null;
					navTarget = hit.point;
					hasNewWaypoint = true;

					// Create a vector from the player to the point on the floor the raycast from the mouse hit.
					Vector3 playerToMouse = hit.point - transform.position;

					// Ensure the vector is entirely along the floor plane.
					playerToMouse.y = 0f;

					// Create a quaternion (rotation) based on looking down the vector from the player to the mouse.
					Quaternion newRotation = Quaternion.LookRotation(playerToMouse);

					// Set the player's rotation to this new rotation.
					rb.MoveRotation(newRotation);
				}
			}
		}

		bool isMoving = false;
		if (navAgent.remainingDistance > 0.2f)
		{
			// animate movement
			isMoving = true;
		}
		PlayerAnimator.SetBool("IsMoving", isMoving);

	}

}
