﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerHealthSlider : MonoBehaviour
{

	public Slider HealthSlider;

	public void SetSliderValue(float value)
	{

		if (HealthSlider != null)
		{ 
			HealthSlider.value = value;
		}

	}

}
