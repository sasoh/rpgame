﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{

	PlayerHealthSlider healthSlider;

	float fullHealth;
	float health;
	public float Health
	{
		get
		{
			return health;
		}
		set
		{
			health = value;

			Debug.Log("health value " + health);
			healthSlider.SetSliderValue(health / fullHealth);
		}
	}

	void Start()
	{

		healthSlider = GetComponent<PlayerHealthSlider>();

		Health = 100.0f;
		fullHealth = Health;

	}

}
