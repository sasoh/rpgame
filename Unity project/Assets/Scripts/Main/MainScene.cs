﻿using UnityEngine;
using System.Collections;

public class MainScene : MonoBehaviour
{

	/// <summary>
	/// Loads choose character screen
	/// </summary>
	public void DidPressStartButton()
	{

		StartCoroutine(GeneralScene.AsyncMapLoad(GeneralScene.SceneMap01));

	}

	/// <summary>
	/// Loads credits scene
	/// </summary>
	public void DidPressCreditsButton()
	{

		StartCoroutine(GeneralScene.AsyncMapLoad(GeneralScene.SceneCredits));
	
	}
	
}
