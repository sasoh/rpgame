# Spire Seekers #

## Creative Brief ##
Centuries have passed since the great mage way that nearly devastated the land. The towns and enclaves that managed to survive transformed into kingdoms eager for power. 
Meanwhile adventurers, known as Spire Seekers, roam the land in search of buried spires from the ancient times. If the legends is to be believed they who find a spire and manage to reach 

## Mechanics Brief ##
The player controls a character whose goal it is to clear floors in a spire, earning XP, currency and items. 

* Each hero has one Active skill he can use in combat
* * The skill starts charged and has a cool down period
* * TBD - additional active skills appear once the active skill is used and could create a chain skill if used within a certain time of the main skill
* Each hero has passive skills that provide advantages in certain fields - TBD
* * Passive skills are performed automatically (regardless if they are flat bonus skills, or have internal cool down)
* Each hero has equipment (Armor, Headgear, Weapon)
* Each hero has items that affect the entire field (allies or enemies)
* * A hero only has 2 slots to equip item types
* * * The number of items of a type are [10]
* * Each item is used by tap (like an active skill)
* * The player cannot re-equip items until he leaves the Spire
* Each hero has 2 slots for allies in battle
* * A mercenary is hired from the city outside
* * Mercenaries have active and passive skills like a hero (but cannot use items, unless they have a passive skill for it)
* * The player cannot change ally structure while he is in the Spire
With each floor cleared the Spire (and town) grows providing the character with new locations for him to buy equipment, acquire skills, etc.

## Combat ##
Battle starts once the character enters the Spire.

* Every Spire level is divided in 3 - X battle zones
* Each battle zone contains X monsters
* * There can only be 5 monsters at once on screen
* * Additional monsters “enter” the scene once there is enough “space”
* * Upon defeating a monster it provides XP and drops currency and items
* * * The currency and items are collected automatically by characters if they are not immediately engaged in combat
* Cool down is only affected by attack (similar to charge up mechanic)
* Once a zone is clear the player is given a short amount of time before being moved automatically to the next zone
* The player can give the order to flee at any time
* Enemies considerably weaker than the party level can also flee
* * Fleeing enemies are considered defeated, but don not provide XP or currency
* * TBD - certain skills or items can prevent enemies to flee

## Hidden Monster Depths ##
Each zone has multiple depth layers

* The character can only interact with one depth layer at a time
* Monsters of other layers are only visible as shadows or spark particle effects
* * The character needs to be equipped with specific armor to access a deeper depth layer

## City Development Brief ##
The city initially starts as a spire/tower trip in the middle of mound/field. As the player clears more levels the spire rises and with it reveals more structures attached to it.

Structures are divided in 2 categories: spire and built

* Spire structures are part of the spire and introduce new mechanics or activities for the player
* * Impressive architecture, ancient symbols, etc
* * Some spire structures are connected deeper down (the connection not apparent until the spire rises to a certain level.
* Built structures have no connection to the spire and provide variety (as well as house heroes)
* * More medieval/rough appearance than spire architecture

Upon being created every building contains at least one character

* Characters in Spire structures provide services (blacksmith, healer, etc)
* All characters can be hires as assistants
* * If the character is defeated he is wounded for a set amount of time and cannot perform any services until healed
* * Potions (and out game currency models) can speed up recovery

Every X levels the spire reveals a new entrance allowing him to go to lower dungeons faster.

## Characters ##
The player initially chooses the main character with which to start the game.

* After that point he cannot change his main character
* The player can start a new game (under a different save slot) with a new character
* * Players can use items o act as short cuts between characters - TBD
* * * Increase the Spire level only
* TBD - certain characters can learn to perform combos with other characters
* * When a combo is performed all characters involved are considered as having used their active skill

When characters acquire enough XP they gain an XP level

* Upon obtaining a new level the character’s health is restored to maximum
* Each XP level allows the character to learn specific skills or increase stats
* * Equipment does NOT require specific character levels

## Character Creation Template ##
Every Character in the game is composed by superimposing of a serious of elements:

* Body Frame
* * Arms
* * * Hand
* * * Weapon
* * * Shield
* * Legs
* * * Foot
* Head
* * Face
* * Eyes
* * Mouth
* * Nose
* * Hair

## Unique Element ##
2D hair fluidity

* Hair constantly moves according to situation and environment

---
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)